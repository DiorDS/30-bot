from typing import Annotated

from pydantic import PostgresDsn, RedisDsn, SecretStr, UrlConstraints
from pydantic_core import Url
from pydantic_settings import BaseSettings

NatsDsn = Annotated[
    Url,
    UrlConstraints(
        allowed_schemes=["nats", "tls"],
        default_host="localhost",
        default_port=4222,
    ),
]


class Settings(BaseSettings):
    bot_token: SecretStr
    postgres_dsn: PostgresDsn
    redis_dsn: RedisDsn
    nats_dsn: NatsDsn
    admin_ids_raw: str

    @property
    def admin_ids(self) -> tuple:
        return tuple(map(int, self.admin_ids_raw.split(",")))

    class Config:
        env_file = ".env"


config = Settings()
