from aiogram.filters.callback_data import CallbackData
from aiogram.types import InlineKeyboardMarkup
from aiogram.utils.keyboard import InlineKeyboardBuilder


class SelectLang(CallbackData, prefix="lang"):
    lang: str


def get_select_lang_kb() -> InlineKeyboardMarkup:
    builder = InlineKeyboardBuilder()
    builder.button(text="English", callback_data=SelectLang(lang="en"))
    builder.button(text="Русский", callback_data=SelectLang(lang="ru"))
    return builder.as_markup()
