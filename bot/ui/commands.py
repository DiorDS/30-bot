from aiogram.types import BotCommand


def get_default_commands() -> list[BotCommand]:
    return [
        BotCommand(command="start", description="Перезапустить бота"),
        BotCommand(command="schedule", description="Получить расписание уроков"),
        BotCommand(command="help", description="Информация"),
        BotCommand(command="terms", description="Условия использования"),
        BotCommand(command="subscribe", description="Подписаться"),
        BotCommand(command="unsubscribe", description="Отписаться"),
        BotCommand(command="lang", description="Выбор языка"),
    ]
