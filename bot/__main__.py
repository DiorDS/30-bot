import asyncio
import logging
from pathlib import Path

from aiogram import Bot, Dispatcher
from aiogram.fsm.storage.redis import RedisStorage
from redis.asyncio import from_url
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine

from services.mail import run

from .config import config
from .handlers import get_routers
from .middlewares import (AnalyticsMiddleware, DbSessionMiddleware,
                          I18nMiddleware, ThrottlingMiddleware)
from .ui import get_default_commands
from .utils.localisation import get_translator_hub

logger = logging.getLogger(__name__)


async def setup(bot: Bot, dp: Dispatcher, session_pool: async_sessionmaker) -> None:
    project_root = Path(__file__).parent.parent

    await bot.delete_webhook(drop_pending_updates=True)
    await bot.set_my_commands(get_default_commands())

    db_session_middleware: DbSessionMiddleware = DbSessionMiddleware(session_pool)
    locale: I18nMiddleware = I18nMiddleware(
        session_pool, get_translator_hub(project_root / "locale")
    )

    dp.message.outer_middleware(
        ThrottlingMiddleware(await from_url(str(config.redis_dsn)))
    )
    dp.message.middleware(db_session_middleware)
    dp.callback_query.middleware(db_session_middleware)
    dp.message.middleware(AnalyticsMiddleware())
    dp.callback_query.middleware(AnalyticsMiddleware())
    dp.message.middleware(locale)
    dp.callback_query.middleware(locale)

    dp.include_routers(*get_routers())


async def main() -> None:
    logging.basicConfig(
        level=logging.INFO,
        format="%(levelname)-8s [%(asctime)s] :: %(name)s : %(message)s",
    )

    engine = create_async_engine(str(config.postgres_dsn))
    session_pool = async_sessionmaker(engine, expire_on_commit=False)

    bot = Bot(config.bot_token.get_secret_value())
    dp = Dispatcher(storage=RedisStorage.from_url(str(config.redis_dsn)))
    dp["config"] = config

    await setup(bot, dp, session_pool)
    logger.warning("Starting mailing service...")

    logger.warning("Starting bot...")

    await asyncio.gather(
        dp.start_polling(bot, allowed_updates=dp.resolve_used_update_types()),
        run(config, bot),
    )


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except (SystemExit, KeyboardInterrupt):
        logger.warning("Bot stopped")
