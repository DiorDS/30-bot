from datetime import date as date_type
from datetime import datetime
from enum import StrEnum

from sqlalchemy import func
from sqlalchemy.dialects.postgresql import (ARRAY, BIGINT, BOOLEAN, DATE,
                                            INTEGER, SMALLINT, TIMESTAMP,
                                            VARCHAR)
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column


class Location(StrEnum):
    SHEVCHENKO = "ул. Шевченко"
    SREDNY = "Средний пр."


class Base(DeclarativeBase):
    pass


class User(Base):
    __tablename__ = "users"

    id: Mapped[int] = mapped_column(BIGINT, primary_key=True, autoincrement=False)
    timestamp: Mapped[datetime] = mapped_column(
        TIMESTAMP, nullable=False, server_default=func.now()
    )
    lang: Mapped[str] = mapped_column(VARCHAR, default="ru", nullable=True)
    is_subscriber: Mapped[bool] = mapped_column(BOOLEAN, default=False)


class DefaultDay(Base):
    __tablename__ = "schedule"

    day_id: Mapped[int] = mapped_column(SMALLINT, primary_key=True, autoincrement=False)
    lessons: Mapped[list[str]] = mapped_column(ARRAY(INTEGER), nullable=False)
    location: Mapped[Location] = mapped_column(
        VARCHAR, nullable=False, default=Location.SREDNY
    )


class Day(Base):
    __tablename__ = "days"

    # id: Mapped[int] = mapped_column(BIGINT, primary_key=True, autoincrement=True)
    day_id: Mapped[int] = mapped_column(SMALLINT, nullable=False)
    date: Mapped[date_type] = mapped_column(
        DATE, primary_key=True, nullable=False, unique=True
    )
    lessons: Mapped[list[str]] = mapped_column(ARRAY(INTEGER), nullable=False)
    location: Mapped[Location] = mapped_column(
        VARCHAR, nullable=False, default=Location.SREDNY
    )


class Lesson(Base):
    __tablename__ = "lessons"

    id: Mapped[int] = mapped_column(INTEGER, primary_key=True)
    name: Mapped[str] = mapped_column(VARCHAR, nullable=False)
    teacher: Mapped[str] = mapped_column(VARCHAR, nullable=False)
    location: Mapped[Location] = mapped_column(
        VARCHAR, nullable=False, default=Location.SREDNY
    )
    room: Mapped[str] = mapped_column(VARCHAR, nullable=False)


class Schedule(Base):
    __tablename__ = "schedules"

    id: Mapped[int] = mapped_column(INTEGER, primary_key=True)
    img_file_id: Mapped[str] = mapped_column(VARCHAR(90), nullable=False)
    creation_date: Mapped[date_type] = mapped_column(DATE, nullable=False)


class Statistics(Base):
    __tablename__ = "statistics"

    event_uid: Mapped[int] = mapped_column(
        INTEGER, primary_key=True, autoincrement=True
    )
    event_group: Mapped[str] = mapped_column(VARCHAR, nullable=False)
    actor_id: Mapped[str] = mapped_column(VARCHAR(8), nullable=False)
    timestamp: Mapped[datetime] = mapped_column(
        TIMESTAMP, nullable=False, server_default=func.now()
    )
    value: Mapped[int] = mapped_column(VARCHAR)
    handler_time: Mapped[int] = mapped_column(INTEGER)


class Mailing(Base):
    # feature
    __tablename__ = "mailing"

    id: Mapped[int] = mapped_column(INTEGER, primary_key=True, autoincrement=True)
    message_id: Mapped[int] = mapped_column(INTEGER)
    content_hash: Mapped[int] = mapped_column(VARCHAR, nullable=False)
    timestamp: Mapped[datetime] = mapped_column(
        TIMESTAMP, nullable=False, server_default=func.now()
    )
