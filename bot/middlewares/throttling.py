from typing import Any, Awaitable, Callable, Dict

from aiogram import BaseMiddleware
from aiogram.types import Message


class ThrottlingMiddleware(BaseMiddleware):
    def __init__(self, session):
        self.session = session

    async def __call__(
        self,
        handler: Callable[[Message, Dict[str, Any]], Awaitable[Any]],
        event: Message,
        data: Dict[str, Any],
    ) -> Any:
        if await self.session.get(f"throttling_{event.chat.id}"):
            return
        await self.session.setex(f"throttling_{event.chat.id}", 1, 1)
        return await handler(event, data)
