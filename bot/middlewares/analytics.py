from time import perf_counter
from typing import Any, Awaitable, Callable, Dict

from aiogram import BaseMiddleware
from aiogram.dispatcher.flags import get_flag
from aiogram.types import Message, User
from sqlalchemy.ext.asyncio import AsyncSession

from ..utils import register_event


class AnalyticsMiddleware(BaseMiddleware):
    async def __call__(
        self,
        handler: Callable[[Message, Dict[str, Any]], Awaitable[Any]],
        event: Message,
        data: Dict[str, Any],
    ) -> Any:
        start = perf_counter()
        event_group: str = get_flag(data, "event_group", default="message")
        value: str = get_flag(data, "value", default="")
        session: AsyncSession = data.get("session")
        user: User = data.get("event_from_user")

        result = await handler(event, data)

        await register_event(
            event_group, user.id, session, value, (perf_counter() - start) * 1000
        )

        return result
