from typing import Any, Awaitable, Callable, Dict

from aiogram import BaseMiddleware
from aiogram.types import CallbackQuery, Message, User
from fluentogram import TranslatorHub
from sqlalchemy.ext.asyncio import async_sessionmaker

from ..db import User as User_


class I18nMiddleware(BaseMiddleware):
    def __init__(
        self, session_pool: async_sessionmaker, translator_hub: TranslatorHub
    ) -> None:
        self.session_pool = session_pool
        self.t_hub = translator_hub

    async def __call__(
        self,
        handler: Callable[[Message, Dict[str, Any]], Awaitable[Any]],
        event: Message | CallbackQuery,
        data: Dict[str, Any],
    ) -> Any:
        user: User = data.get("event_from_user")

        async with self.session_pool() as session:
            db_user: User_ | None = await session.get(User_, user.id)
            if db_user is None:
                db_user = User_(id=user.id, lang=user.language_code)
                session.add(db_user)
                await session.commit()
                data["is_new_user"] = True

            elif isinstance(event, CallbackQuery):
                if (callback_data := event.data) in ("lang:en", "lang:ru"):
                    db_user.lang = callback_data.split(":")[1]
                    await session.commit()

        data["i18n"] = self.t_hub.get_translator_by_locale(db_user.lang)
        return await handler(event, data)
