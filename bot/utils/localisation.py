from logging import getLogger
from pathlib import Path

from fluent_compiler.bundle import FluentBundle
from fluentogram import FluentTranslator, TranslatorHub


def get_translator_hub(locales_path: Path) -> TranslatorHub:
    en_paths: list[Path] = list((locales_path / "en").glob("*.ftl"))
    ru_paths: list[Path] = list((locales_path / "ru").glob("*.ftl"))

    logger = getLogger(__name__)

    logger.info("Loading locales from %s", locales_path)
    logger.info("ru paths: %s", ru_paths)
    logger.info("en paths: %s", en_paths)

    translator_hub = TranslatorHub(
        {"ru": ("ru", "en"), "en": ("en",)},
        [
            FluentTranslator(
                "en",
                translator=FluentBundle.from_files("en-US", en_paths),
            ),
            FluentTranslator(
                "ru",
                translator=FluentBundle.from_files("ru-RU", ru_paths),
            ),
        ],
    )

    return translator_hub
