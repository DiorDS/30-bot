from datetime import date
from logging import getLogger

from aiogram.types import BufferedInputFile, Message
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from renderer import Renderer

from ..db import Day, DefaultDay, Lesson

DAYS = {
    0: "Понедельник",
    1: "Вторник",
    2: "Среда",
    3: "Четверг",
    4: "Пятница",
    5: "Суббота",
    6: "Воскресенье",
}


async def update(message: Message, session: AsyncSession) -> str:
    logger = getLogger(__name__)
    schedule: DefaultDay | None = await session.get(DefaultDay, date.today().weekday())

    if custom_schedule := await session.scalar(
        select(Day).where(Day.date == date.today())
    ):
        schedule = custom_schedule

    elif schedule is None:
        raise ValueError("No schedule for today")

    lessons = schedule.lessons
    subjects: list[Lesson] = (
        await session.scalars(
            select(Lesson).where(Lesson.id.in_(lessons)).order_by(Lesson.id)
        )
    ).all()  # type: ignore

    subjects = {x.id: x for x in subjects}

    logger.info(lessons)
    logger.info(subjects)

    data = {
        "font": "JetBrains Mono",
        "bg_color": "525fe1",
        "text_color": "fff6f4",
        "warn_color": "ff0060",
        "cabinets": [subjects[x].room for x in schedule.lessons],
        "subjects": [subjects[x].name for x in schedule.lessons],
        "round": round,
        "len": len,
        "warns": [None] * len(subjects),
        "day": DAYS.get(schedule.day_id, "Неизвестный день"),
        "loc": schedule.location,
    }

    logger.info(data)

    file = await Renderer("./assets/templates").render_to_io(data, "schedule.svg")

    msg = await message.answer_photo(
        BufferedInputFile(file.read(), filename="schedule.png")
    )
    file_id = msg.photo[-1].file_id  # type: ignore

    logger.info(f"Schedule for {date.today()} id is {file_id}")

    return file_id
