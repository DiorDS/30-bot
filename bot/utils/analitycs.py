import hashlib

from sqlalchemy.ext.asyncio import AsyncSession

from ..db import Statistics


async def encode_user(user_id: int) -> str:
    return hashlib.sha256(str(user_id).encode("utf-8")).hexdigest()[:8]


async def register_event(
    event_group: str,
    event_from_id: int,
    session: AsyncSession,
    value="",
    handler_time=0,
) -> None:
    session.add(
        Statistics(
            event_group=event_group,
            actor_id=await encode_user(event_from_id),
            value=value,
            handler_time=handler_time,
        )
    )
    await session.commit()
