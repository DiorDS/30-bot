from sqlalchemy.ext.asyncio import AsyncSession

from ..db import Lesson


async def lesson_exists(lesson_id: int, session: AsyncSession) -> bool:
    return not not await session.get(Lesson, lesson_id)
