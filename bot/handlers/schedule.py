from datetime import date
from logging import getLogger

from aiogram import Router
from aiogram.filters import Command
from aiogram.types import Message
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..db import Schedule
from ..utils import update

router = Router()


@router.message(
    Command("schedule", "today"), flags={"event_group": "command", "value": "schedule"}
)
async def daily_schedule(message: Message, session: AsyncSession) -> None:
    logger = getLogger(__name__)
    today = date.today()
    img_file_id = await session.scalar(
        select(Schedule.img_file_id).where(Schedule.creation_date == today)
    )

    if not img_file_id:
        logger.info("No schedule for today, creating...")

        file_id = await update(message, session)
        session.add(Schedule(creation_date=today, img_file_id=file_id))
        await session.commit()

        logger.info("Schedule for today created")

    else:
        await message.answer_photo(img_file_id)
