from datetime import date
from json import dumps

from aiogram import Bot, F, Router
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup
from aiogram.types import Message
from nats.aio.client import Client as NATS
from sqlalchemy import insert, select, update
from sqlalchemy.ext.asyncio import AsyncSession

from bot.db import Day, DefaultDay, Location, User
from bot.filters import AdminFilter
from bot.ui import get_accept_keyboard, remove
from bot.utils import lesson_exists

router = Router()
router.message.filter(AdminFilter(is_admin=True))


class SetDefaultDayStates(StatesGroup):
    id_select = State()
    lessons_select = State()
    location_select = State()
    check = State()


@router.message(
    Command("set_default_day"),
    flags={"event_group": "admin_fsm_enter", "value": "set_default_day"},
)
async def set_default_day_cmd(message: Message, state: FSMContext) -> None:
    await message.answer("Хорошо, напишите номер дня (0-6)")
    await state.set_state(SetDefaultDayStates.id_select)


@router.message(
    SetDefaultDayStates.id_select,
    flags={"event_group": "admin_fsm_message", "value": "set_default_day"},
)
async def set_default_day_id(message: Message, state: FSMContext):
    await state.update_data({"day_id": int(message.text)})
    await message.answer("Хорошо, напишите через пробел id всех уроков")
    await state.set_state(SetDefaultDayStates.lessons_select)


# noinspection PyShadowingNames
@router.message(
    SetDefaultDayStates.lessons_select,
    flags={"event_group": "admin_fsm_message", "value": "set_default_day"},
)
async def set_default_day_lessons(
    message: Message, state: FSMContext, session: AsyncSession
):
    lessons_ids = tuple(map(int, message.text.split(" ")))
    for lesson in set(lessons_ids):
        if not await lesson_exists(lesson, session):
            await message.answer(
                f"Урок с id: {lesson} не найден, напишите через пробел id всех уроков"
            )
            return

    await state.update_data({"lessons": lessons_ids})
    await message.answer(
        f"""Хорошо, напишите номер площадки
0 - {Location.SREDNY}\n1 - {Location.SHEVCHENKO}"""
    )
    await state.set_state(SetDefaultDayStates.location_select)


@router.message(
    SetDefaultDayStates.location_select,
    flags={"event_group": "admin_fsm_checkout", "value": "set_default_day"},
)
async def set_default_day_location(message: Message, state: FSMContext):
    await state.update_data(
        {"location": Location.SREDNY if message.text == "0" else Location.SHEVCHENKO}
    )
    data = await state.get_data()
    await message.answer(
        f"""\
Хорошо, проверьте данные:
Номер дня: {data.get('day_id')}
ID уроков: {data.get('lessons')}
Корпус: {data.get('location')}""",
        reply_markup=get_accept_keyboard(),
    )
    await state.set_state(SetDefaultDayStates.check)


@router.message(
    SetDefaultDayStates.check,
    F.text.lower() == "ок",
    flags={"event_group": "admin_fsm_finish_ok", "value": "set_default_day"},
)
async def set_default_day_check_ok(
    message: Message, state: FSMContext, session: AsyncSession
):
    data = await state.get_data()
    result: DefaultDay | None = await session.get(DefaultDay, data.get("day_id"))
    if result:
        day_id: int = await session.scalar(
            update(DefaultDay)
            .values(
                day_id=data.get("day_id"),
                lessons=data.get("lessons"),
                location=data.get("location"),
            )
            .returning(DefaultDay.day_id)
        )
    else:
        day_id: int = await session.scalar(
            insert(DefaultDay)
            .values(
                day_id=data.get("day_id"),
                lessons=data.get("lessons"),
                location=data.get("location"),
            )
            .returning(DefaultDay.day_id)
        )

    await session.commit()
    await message.answer(f"День c id: {day_id} изменен", reply_markup=remove())

    await state.clear()


@router.message(
    SetDefaultDayStates.check,
    F.text.lower() == "отмена",
    flags={"event_group": "admin_fsm_finish_cancel", "value": "set_default_day"},
)
async def set_default_day_check_cancel(message: Message, state: FSMContext):
    await message.answer("День не изменен", reply_markup=remove())
    await state.clear()


class SetDayStates(StatesGroup):
    day_input = State()
    lessons_select = State()
    location_select = State()
    check = State()
    notify_select = State()


@router.message(
    Command("set_day"), flags={"event_group": "admin_fsm_enter", "value": "set_day"}
)
async def set_day_cmd(message: Message, state: FSMContext) -> None:
    await message.answer("Хорошо, введите дату (2023-06-30)")
    await state.set_state(SetDayStates.day_input)


@router.message(
    SetDayStates.day_input,
    flags={"event_group": "admin_fsm_message", "value": "set_day"},
)
async def set_day_date(message: Message, state: FSMContext):
    await state.update_data(
        {"date": message.text, "day_id": date.fromisoformat(message.text).weekday()}
    )
    await message.answer("Хорошо, напишите через пробел id всех уроков")
    await state.set_state(SetDayStates.lessons_select)


@router.message(
    SetDayStates.lessons_select,
    flags={"event_group": "admin_fsm_message", "value": "set_day"},
)
async def set_day_lessons(message: Message, state: FSMContext, session: AsyncSession):
    lesson_ids: tuple[int] = tuple(map(int, message.text.split(" ")))
    for lesson in set(lesson_ids):
        if not await lesson_exists(lesson, session):
            await message.answer(
                f"Урок с id: {lesson} не найден, напишите через пробел id всех уроков"
            )
            return

    await state.update_data({"lessons": lesson_ids})
    await message.answer("Хорошо, будем оповещать об изменениях\n0 - нет\n1 - да")
    await state.set_state(SetDayStates.notify_select)


@router.message(
    SetDayStates.notify_select,
    flags={"event_group": "admin_fsm_message", "value": "set_day"},
)
async def set_day_notify(message: Message, state: FSMContext):
    await state.update_data({"notify": int(message.text)})
    await message.answer(
        f"""Хорошо, напишите номер площадки
0 - {Location.SREDNY}\n1 - {Location.SHEVCHENKO}"""
    )
    await state.set_state(SetDayStates.location_select)


@router.message(
    SetDayStates.location_select,
    flags={"event_group": "admin_fsm_checkout", "value": "set_day"},
)
async def set_day_checkout(message: Message, state: FSMContext):
    await state.update_data(
        {"location": Location.SREDNY if message.text == "0" else Location.SHEVCHENKO}
    )
    data = await state.get_data()
    await message.answer(
        f"""\
Хорошо, проверьте данные:
Дата: {data.get('date')}
Номер дня: {data.get('day_id')}
ID уроков: {data.get('lessons')}
Корпус: {data.get('location')}
Уведомление об изменениях: {data.get('notify')}""",
        reply_markup=get_accept_keyboard(),
    )
    await state.set_state(SetDayStates.check)


@router.message(
    SetDayStates.check,
    F.text.lower() == "ок",
    flags={"event_group": "admin_fsm_finish_ok", "value": "set_day"},
)
async def set_day_check_ok(
    message: Message, state: FSMContext, session: AsyncSession, bot: Bot, config
):
    result: Day | None = await session.scalar(
        select(Day).where(
            Day.date == date.fromisoformat((await state.get_data()).get("date"))
        )
    )
    data = await state.get_data()

    data["date"] = date.fromisoformat(data.get("date"))

    if result:
        day_id: int = await session.scalar(
            update(Day)
            .where(Day.date == data.get("date"))
            .values(
                day_id=data.get("day_id"),
                date=data.get("date"),
                lessons=data.get("lessons"),
                location=data.get("location"),
            )
            .returning(Day.day_id)
        )
    else:
        day_id: int = await session.scalar(
            insert(Day)
            .values(
                day_id=data.get("day_id"),
                date=data.get("date"),
                lessons=data.get("lessons"),
                location=data.get("location"),
            )
            .returning(Day.day_id)
        )

    await session.commit()
    await message.answer(f"День c id: {day_id} изменен", reply_markup=remove())
    await state.clear()

    if data.get("notify") == 1:
        users: list[User] = (
            await session.scalars(
                select(User).where(User.is_subscriber == True).limit(None)
            )
        ).all()  # type: ignore

        nc = NATS()
        await nc.connect(str(config.nats_dsn()))
        js = nc.jetstream()

        message_id = (
            await bot.send_message(
                message.from_user.id,
                f"**Изменения в расписание!**\n"
                f"Дата: {data.get('date')}\n"
                f"Кол-во уроков: {len(data.get('lessons'))}\n"
                f"Корпус: {data.get('location')}",
                parse_mode="Markdown",
            )
        ).message_id

        for user in users:
            payload = {
                "target": user.id,
                "message": {"message_id": message_id, "chat_id": message.chat.id},
            }
            await js.publish("mailing", payload=dumps(payload).encode("utf-8"))

        await message.answer(f"Рассылка запущена на {len(users)} пользователей.")


@router.message(
    SetDayStates.check,
    F.text.lower() == "отмена",
    flags={"event_group": "admin_fsm_finish_cancel", "value": "set_day"},
)
async def set_day_check_cancel(message: Message, state: FSMContext):
    await message.answer("День не изменен", reply_markup=remove())
    await state.clear()
