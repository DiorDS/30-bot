from aiogram import Router


def get_admin_routers() -> tuple[Router]:
    from bot.handlers.admin.commands import router as commands_router
    from bot.handlers.admin.days import router as days_router
    from bot.handlers.admin.lessons import router as lessons_router

    return (
        commands_router,
        days_router,
        lessons_router,
    )
