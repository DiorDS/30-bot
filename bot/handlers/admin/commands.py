from datetime import date

from aiogram import Router
from aiogram.enums import ParseMode
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import Message
from prettytable import PrettyTable
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from bot.db import Schedule, User
from bot.filters import AdminFilter
from bot.utils import update as update_schedule

router = Router()
router.message.filter(AdminFilter(is_admin=True))


@router.message(
    Command("ahelp", "amenu"), flags={"event_group": "admin_command", "value": "ahelp"}
)
async def admin_help_cmd(message: Message) -> None:
    await message.answer(
        """Список админ команд:
/add_lesson - добавить урок
/set_default_day - добавить/изменить день по умолчанию
/set_day - изменить день
/mail - отправить сообщение
/stats - статистика
/extended_stats - расширенная статистика
/time_stats - статистика использования по часам
/update - обновить список уроков
/lessons - список уроков
/subscribers - список подписчиков
/cancel - отмена текущего действия

/ahelp - помощь
"""
    )


@router.message(Command("cancel"), flags={"event_group": "admin_fsm_cancel"})
async def cancel_cmd(message: Message, state: FSMContext):
    await message.answer("Текущее действие отменено.")

    await state.clear()


@router.message(
    Command("update"), flags={"event_group": "admin_command", "value": "update"}
)
async def update_cmd(message: Message, session: AsyncSession) -> None:
    file_id = await update_schedule(message, session)
    if (
        await session.scalar(
            select(Schedule.img_file_id).where(Schedule.creation_date == date.today())
        )
        is not None
    ):
        await session.execute(
            update(Schedule)
            .where(Schedule.creation_date == date.today())
            .values(img_file_id=file_id)
        )
    else:
        session.add(Schedule(creation_date=date.today(), img_file_id=file_id))

    await session.commit()
    await message.answer("Обновлено")


@router.message(
    Command("subscribers"),
    flags={"event_group": "admin_command", "value": "subscribers"},
)
async def subscribers_cmd(message: Message, session: AsyncSession):
    users: list[User] = (
        await session.scalars(select(User).where(User.is_subscriber == True).limit(20))
    ).all()

    table = PrettyTable(["ID"])

    for user in users:
        table.add_row([user.id])

    await message.answer(
        f"<b>Подписчики:</b>\n\n<pre>{table}</pre>", parse_mode=ParseMode.HTML
    )
