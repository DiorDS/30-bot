import re

from aiogram import F, Router
from aiogram.enums import ParseMode
from aiogram.filters import Command, CommandObject
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup
from aiogram.types import Message
from prettytable import PrettyTable
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from bot.db import Lesson, Location
from bot.filters import AdminFilter
from bot.ui import get_accept_keyboard, remove

router = Router()
router.message.filter(AdminFilter(is_admin=True))


class LessonAddStates(StatesGroup):
    name_select = State()
    room_select = State()
    location_select = State()
    teacher_select = State()
    check = State()


@router.message(
    Command("add_lesson"),
    flags={"event_group": "admin_fsm_enter", "value": "add_lesson"},
)
async def add_lesson_cmd(message: Message, state: FSMContext) -> None:
    await message.answer("Хорошо, напишите название урока")
    await state.set_state(LessonAddStates.name_select)


@router.message(
    LessonAddStates.name_select,
    flags={"event_group": "admin_fsm_message", "value": "add_lesson"},
)
async def add_lesson_name(message: Message, state: FSMContext):
    await state.update_data({"name": message.text})
    await message.answer("Хорошо, напишите кабинет где проходит урок")
    await state.set_state(LessonAddStates.room_select)


@router.message(
    LessonAddStates.room_select,
    flags={"event_group": "admin_fsm_message", "value": "add_lesson"},
)
async def add_lesson_room(message: Message, state: FSMContext):
    await state.update_data({"room": message.text})
    await message.answer(
        f"""Хорошо, напишите номер площадки где проходит урок
0 - {Location.SREDNY}\n1 - {Location.SHEVCHENKO}"""
    )
    await state.set_state(LessonAddStates.location_select)


@router.message(
    LessonAddStates.location_select,
    flags={"event_group": "admin_fsm_message", "value": "add_lesson"},
)
async def add_lesson_location(message: Message, state: FSMContext):
    await state.update_data(
        {"location": Location.SREDNY if message.text == "0" else Location.SHEVCHENKO}
    )
    await message.answer("Хорошо, напишите учителя ведущего урок")
    await state.set_state(LessonAddStates.teacher_select)


@router.message(
    LessonAddStates.teacher_select,
    flags={"event_group": "admin_fsm_checkout", "value": "add_lesson"},
)
async def add_lesson_teacher(message: Message, state: FSMContext):
    await state.update_data({"teacher": message.text})
    data = await state.get_data()
    await message.answer(
        f"""\
Хорошо, проверьте данные:
Наименование: {data.get('name')}
Кабинет: {data.get('room')}
Корпус: {data.get('location')}
Учитель: {data.get('teacher')}""",
        reply_markup=get_accept_keyboard(),
    )
    await state.set_state(LessonAddStates.check)


@router.message(
    LessonAddStates.check,
    F.text.lower() == "ок",
    flags={"event_group": "admin_fsm_finish_ok", "value": "add_lesson"},
)
async def add_lesson_check_ok(
    message: Message, state: FSMContext, session: AsyncSession
):
    data = await state.get_data()
    lesson = Lesson(
        name=data.get("name"),
        teacher=data.get("teacher"),
        room=data.get("room"),
        location=data.get("location"),
    )
    session.add(lesson)
    await session.commit()
    await message.answer(f"Урок добавлен c id: {lesson.id}", reply_markup=remove())
    await state.clear()


@router.message(
    LessonAddStates.check,
    F.text.lower() == "отмена",
    flags={"event_group": "admin_fsm_finish_cancel", "value": "add_lesson"},
)
async def add_lesson_check_cancel(message: Message, state: FSMContext):
    await message.answer("Урок не добавлен", reply_markup=remove())
    await state.clear()


@router.message(
    Command("lessons"), flags={"event_group": "admin_command", "value": "lessons"}
)
async def lessons_cmd(message: Message, command: CommandObject, session: AsyncSession):
    pattern = r"(\w+)=(.*?);"
    filters = []
    if command.args:
        for key, value in dict(re.findall(pattern, command.args)).items():
            filters.append(getattr(Lesson, key) == value)

    lesson_ids = (await session.scalars(select(Lesson).where(*filters))).all()

    table = PrettyTable(["ID", "Название", "Кабинет", "Корпус", "Учитель"])
    for lesson in lesson_ids:
        table.add_row(
            [lesson.id, lesson.name, lesson.room, lesson.location, lesson.teacher]
        )

    await message.answer(
        f"<b>Список уроков:</b>\n\n<pre>{table}</pre>",
        parse_mode=ParseMode.HTML,
    )
