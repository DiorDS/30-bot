import time

from aiogram import F, Router
from aiogram.enums import ParseMode
from aiogram.filters import Command, CommandStart
from aiogram.types import CallbackQuery, Message
from fluentogram import TranslatorRunner
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from ..db import User
from ..ui import get_select_lang_kb

router = Router()


@router.message(CommandStart(), flags={"event_group": "start"})
async def start_cmd(
    message: Message, session: AsyncSession, i18n: TranslatorRunner
) -> None:
    user = await session.get(User, message.from_user.id)
    if user is None:
        user = User(id=message.from_user.id)
        session.add(user)
        await session.commit()

    await message.answer(
        i18n.get("start-message")
    )  # TODO: сделать приветственное сообщение


@router.message(Command("lang"), flags={"value": "lang", "event_group": "command"})
async def lang_cmd(message: Message, i18n: TranslatorRunner) -> None:
    await message.answer(i18n.get("select-lang"), reply_markup=get_select_lang_kb())


@router.callback_query(
    F.data.in_(("lang:en", "lang:ru")),
    flags={"value": "lang_post", "event_group": "command"},
)
async def lang_post(callback: CallbackQuery, i18n: TranslatorRunner) -> None:
    await callback.answer(i18n.get("changed-lang"))


@router.message(Command("subscribe"), flags={"event_group": "subscribe"})
async def subscribe_cmd(
    message: Message, session: AsyncSession, i18n: TranslatorRunner
) -> None:
    query = select(User).where(User.id == message.from_user.id)
    usr: User = await session.scalar(query)

    if usr.is_subscriber:
        await message.answer(i18n.get("already-subscribed"))
        return

    await session.execute(
        update(User).where(User.id == message.from_user.id).values(is_subscriber=True)
    )
    await session.commit()

    await message.answer(i18n.get("subscribed"))


@router.message(Command("unsubscribe"), flags={"event_group": "unsubscribe"})
async def unsubscribe_cmd(
    message: Message, session: AsyncSession, i18n: TranslatorRunner
) -> None:
    query = select(User).where(User.id == message.from_user.id)
    usr: User = await session.scalar(query)

    if not usr.is_subscriber:
        await message.answer(i18n.get("already-unsubscribed"))
        return

    await session.execute(
        update(User).where(User.id == message.from_user.id).values(is_subscriber=False)
    )
    await session.commit()

    await message.answer(i18n.get("unsubscribed"))


@router.message(Command("ping"), flags={"event_group": "command", "value": "ping"})
async def ping_cmd(message: Message) -> None:
    start = time.perf_counter_ns()

    reply_message = await message.answer(
        "<code>⏱ Checking ping...</code>", parse_mode=ParseMode.HTML
    )
    end = time.perf_counter_ns()
    ping = (end - start) * 0.000001

    await reply_message.edit_text(
        f"<b>⏱ Ping -</b> <code>{round(ping, 3)}</code> <b>ms</b>",
        parse_mode=ParseMode.HTML,
    )


@router.message(Command("help"), flags={"event_group": "command", "value": "help"})
async def help_cmd(message: Message, i18n: TranslatorRunner) -> None:
    await message.answer(i18n.get("help-message"), parse_mode=ParseMode.HTML)


@router.message(Command("about"), flags={"event_group": "command", "value": "about"})
async def about_cmd(message: Message, i18n: TranslatorRunner) -> None:
    await message.answer(i18n.get("about-message"), parse_mode=ParseMode.HTML)


@router.message(Command("terms"), flags={"event_group": "command", "value": "terms"})
async def terms_cmd(message: Message, i18n: TranslatorRunner) -> None:
    await message.answer(i18n.get("terms-message"), parse_mode=ParseMode.HTML)
