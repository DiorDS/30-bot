def get_routers() -> tuple:
    import features

    from . import admin, common, mail, stats, schedule

    # session_middleware = DbSessionMiddleware(session_pool)
    # schedule.router.message.middleware(session_middleware)
    # admin.router.message.middleware(session_middleware)

    return (
        common.router,
        features.router,
        schedule.router,
        *admin.get_admin_routers(),
        mail.router,
        stats.router,
    )
