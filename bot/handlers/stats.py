import prettytable as pt
from aiogram import Router
from aiogram.enums import ParseMode
from aiogram.filters import Command
from aiogram.types import Message
from sqlalchemy import text
from sqlalchemy.ext.asyncio import AsyncSession

from ..filters import AdminFilter

router = Router()

router.message.filter(AdminFilter(is_admin=True))


@router.message(
    Command("stats"), flags={"event_group": "admin_command", "value": "stats"}
)
async def stats_cmd(message: Message, session: AsyncSession) -> None:
    table: pt.PrettyTable = pt.PrettyTable(["Группа событий", "Использований"])
    table.align["Группа событий"] = "l"

    result = await session.execute(
        text(
            """\
SELECT event_group, COUNT(*) AS event_count
FROM statistics
WHERE timestamp::date = CURRENT_DATE
GROUP BY event_group
ORDER BY event_count DESC"""
        )
    )  # TODO: IMPORTANT REFACTOR

    count = await count_today_calls(session)
    table.add_rows(result)

    await message.answer(
        f"<b>Статистика за сегодня:</b>\nВсего вызовов: {count}\n\n<pre>{table}</pre>",
        parse_mode=ParseMode.HTML,
    )


@router.message(
    Command("extended_stats"),
    flags={"event_group": "admin_command", "value": "extended_stats"},
)
async def extended_stats_cmd(message: Message, session: AsyncSession) -> None:
    table: pt.PrettyTable = pt.PrettyTable(
        ["Группа событий", "Событие", "Использований"]
    )
    table.align["Группа событий"] = "l"
    table.align["Событие"] = "r"

    result = await session.execute(
        text(
            """\
SELECT event_group, value, COUNT(*) AS event_count
FROM statistics
WHERE timestamp::date = CURRENT_DATE
GROUP BY event_group, value
ORDER BY event_count DESC"""
        )
    )  # TODO: IMPORTANT REFACTOR
    count = await count_today_calls(session)

    table.add_rows(result)

    await message.answer(
        f"<b>Статистика за сегодня</b>\nВсего вызовов: {count}\n\n<pre>{table}</pre>",
        parse_mode=ParseMode.HTML,
    )


@router.message(
    Command("time_stats"), flags={"event_group": "admin_command", "value": "time_stats"}
)
async def time_stats_cmd(message: Message, session: AsyncSession) -> None:
    table: pt.PrettyTable = pt.PrettyTable(["Час", "Использований"])
    table.align["Час"] = "l"

    result = await session.execute(
        text(
            """\
SELECT date_trunc('hour', timestamp::time) AS hour_trunc, COUNT(*) AS event_count
FROM statistics
WHERE timestamp::date = CURRENT_DATE
GROUP BY hour_trunc
ORDER BY hour_trunc ASC;"""
        )
    )  # TODO: IMPORTANT REFACTOR
    count = await count_today_calls(session)

    table.add_rows(result)

    await message.answer(
        f"<b>Статистика за сегодня</b>\nВсего вызовов: {count}\n\n<pre>{table}</pre>",
        parse_mode=ParseMode.HTML,
    )


@router.message(
    Command("user_stats"), flags={"event_group": "admin_command", "value": "user_stats"}
)
async def user_stats_cmd(message: Message, session: AsyncSession) -> None:
    table: pt.PrettyTable = pt.PrettyTable(["Тег пользователя", "Использований"])
    table.align["Тег пользователя"] = "l"
    table.align["Использований"] = "r"

    result = await session.execute(
        text(
            """\
SELECT actor_id, COUNT(*) AS event_count
FROM statistics
WHERE timestamp::date = CURRENT_DATE
GROUP BY actor_id
ORDER BY event_count DESC;"""
        )
    )  # TODO: IMPORTANT REFACTOR
    count = await count_today_calls(session)

    table.add_rows(result)

    await message.answer(
        f"<b>Статистика за сегодня</b>\nВсего вызовов: {count}\n\n<pre>{table}</pre>",
        parse_mode=ParseMode.HTML,
    )


async def count_today_calls(session):
    return await session.scalar(
        text("SELECT COUNT(*) FROM statistics WHERE timestamp::date = CURRENT_DATE")
    )
