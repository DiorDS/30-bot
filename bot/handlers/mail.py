import hashlib
from json import dumps

from aiogram import F, Router
from aiogram.enums import ParseMode
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup
from aiogram.types import CallbackQuery, Message
from nats.aio.client import Client as NATS
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from ..config import Settings
from ..db import Mailing, User
from ..filters import AdminFilter
from ..ui import get_accept_kb

router = Router()
router.message.filter(AdminFilter(is_admin=True))


class Mail(StatesGroup):
    input_mail = State()
    checkout = State()


@router.message(
    Command("mail"), flags={"event_group": "admin_fsm_enter", "value": "mail"}
)
async def mail_cmd(message: Message, state: FSMContext) -> None:
    await state.set_state(Mail.input_mail)
    await message.answer("Отправьте сообщение")


@router.message(
    Mail.input_mail, flags={"event_group": "admin_fsm_checkout", "value": "mail"}
)
async def mail_received(message: Message, state: FSMContext) -> None:
    await state.set_state(Mail.checkout)
    await state.update_data(
        message_id=message.message_id,
        message_hash=hashlib.md5(message.html_text.encode("utf-8")).hexdigest(),
    )
    await message.answer(
        f"<b>Сообщение:</b>\n\n{message.html_text}",
        reply_markup=get_accept_kb(),
        parse_mode=ParseMode.HTML,
    )


@router.callback_query(
    Mail.checkout, F.data == "confirm", flags={"event_group": "admin_mail_send_ok"}
)
async def mail_confirmed(
    callback: CallbackQuery,
    state: FSMContext,
    session: AsyncSession,
    config: Settings,
) -> None:
    users: list[User] = (
        await session.scalars(select(User).limit(None))
    ).all()  # type: ignore
    message_id: int = (await state.get_data()).get("message_id")

    session.add(
        Mailing(
            message_id=message_id,
            content_hash=(await state.get_data()).get("message_hash"),
        )
    )
    await session.commit()

    nc = NATS()
    await nc.connect(str(config.nats_dsn))
    js = nc.jetstream()

    for user in users:
        payload = {
            "target": user.id,
            "message": {"message_id": message_id, "chat_id": callback.message.chat.id},
        }
        await js.publish("mailing", payload=dumps(payload).encode("utf-8"))

    await callback.answer(f"Рассылка запущена на {len(users)} пользователей.")


@router.callback_query(
    Mail.checkout, F.data == "cancel", flags={"event_group": "admin_mail_send_cancel"}
)
async def mail_cancelled(callback: CallbackQuery, state: FSMContext) -> None:
    await state.clear()
    await callback.answer()
    await callback.message.edit_text("Действие отменено")
