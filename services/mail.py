from json import loads

from aiogram.exceptions import TelegramRetryAfter
from nats.aio.client import Client as NATS


async def send_message(msg, bot) -> None:
    msg_data = loads(msg.data)
    try:
        await bot.copy_message(
            msg_data["target"],
            msg_data["message"]["chat_id"],
            msg_data["message"]["message_id"],
        )
        await msg.ack()
    except TelegramRetryAfter as e:
        await msg.nak(delay=e.retry_after)
    except:
        await msg.ack()

    # TODO: handle ban


async def run(config, bot) -> None:
    nc = NATS()
    await nc.connect(str(config.nats_dsn))

    js = nc.jetstream()

    sub = await js.subscribe(
        "mailing",
        "bot30",
    )

    async for msg in sub.messages:
        await send_message(msg, bot)
