start-message = Hello, you are using a timetable bot created by PML #30 students.
already-subscribed = You are already subscribed
    if you want to unsubscribe then use /unsubscribe.
subscribed = You have successfully subscribed, 
    if you want to unsubscribe then use /unsubscribe.
unsubscribed = You have successfully unsubscribed!
    if you want to unsubscribe then use /unsubscribe
already-unsubscribed = You are already unsubscribed
    if you want to subscribe then use /subscribe
help-message = 
    This bot was created by #30 PML students to provide information and more =)
    /schedule - get the lesson schedule
    /ping - check the bot`s functionality
    /subscribe - subscribe
    /unsubscribe - unsubscribe
    /terms - terms of use
    /lang - change language

    /about - information about the bot
about-message = 
    The bot`s repository is <a href='https://github.com/dsodx/pml30-bot'>GitHub</a>
    You can show your appreciation to the creators by starring the repository.

    If you have any issues to report or ideas to share, 
    You can do so on the <a href='https://github.com/dsodx/pml30-bot/issues'>issues</a>

    Pull requests are also welcome for more experienced users who want to contribute.
    <span class='tg-spoiler'>/feature - ?????</span>
    
    Tech Stack
    - OS: Ubuntu 22.04 LTS
    - Docker Compose 2.18
    - PostgreSQL 15.3
    - CPython: 3.11
    - Aiogram: 3.0.0b7
    - SQLAlchemy: 2.16
    - Asyncpg: 0.27
terms-message = 
    Terms of Use:
    By using this bot, you agree to our terms and conditions.

    The bot is intended for personal use only and should not be used for commercial purposes without our written permission.
    Transferring, selling, renting, licensing, publishing, distributing, modifying, or creating derivative works based on our bot or its code without our explicit permission is strictly prohibited.
    We are not responsible for any losses or issues arising from the use of this bot.
    By using the bot, you agree to comply with all applicable laws and Telegram rules.
    We reserve the right to modify these terms and conditions at our discretion.
    If you have any questions or issues related to the bot, you can contact us through the contact information provided in the repository on <a href='https://github.com/dsodx/pml30-bot'>GitHub</a>
    Thank you for using our bot!
select-lang =
    Select your language
changed-lang =
    Your language changed succsesful